# -*- coding: utf-8 -*-
"""
Created on Tue May 22 14:38:59 2019

@author: cdany
"""


# Imports necesarios
import numpy as np
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt
#%matplotlib inline
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('ggplot')
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

#cargamos los datos de entrada
data = pd.read_csv("calificaciones1.csv")
#veamos cuantas dimensiones y registros contiene
data.shape
#son 161 registros con 8 columnas. Veamos los primeros registros para tener una idea
data.head()


# Visualizamos rápidamente las caraterísticas de entrada
data.drop(['Ncontrol'],1).hist()
plt.show()

#vamos a Visualizar los datos de entrada
colores=['orange','blue']
tamanios=[30,60]

f1 = data['Proyecto'].values
f2 = data['Ntareas'].values



filtered_data = data[(data['Ntareas'] <= 15) & (data['Proyecto'] <= 50)]

f1 = filtered_data['Proyecto'].values
f2 = filtered_data['Ntareas'].values


asignar=[]
for index, row in filtered_data.iterrows():
    if(row['Proyecto']>50):
        asignar.append(colores[0])
    else:
        asignar.append(colores[1])

plt.scatter(f1, f2, c=asignar, s=tamanios[0])


# Asignamos nuestra variable de entrada X para entrenamiento y las etiquetas Y.
dataX =filtered_data[["Proyecto"]]
X_train = np.array(dataX)
y_train = filtered_data['Ntareas'].values


# Creamos el objeto de Regresión Linear
regr = linear_model.LinearRegression()

# Entrenamos nuestro modelo
regr.fit(X_train, y_train)

# Hacemos las predicciones que en definitiva una línea (en este caso, al ser 2D)
y_pred = regr.predict(X_train)

# Veamos los coeficienetes obtenidos, En nuestro caso, serán la Tangente
print('Coeficiente: \n', regr.coef_)
# Este es el valor donde corta el eje Y (en X=0)
print('Termino independiente: \n', regr.intercept_)
# Error Cuadrado Medio
print("Error cuadrático medio: %.2f" % mean_squared_error(y_train, y_pred))
# Puntaje de Varianza. El mejor puntaje es un 1.0
print('Valor de la Varianza: %.2f' % r2_score(y_train, y_pred))

plt.scatter(X_train[:,0], y_train,  c=asignar, s=tamanios[0])
plt.plot(X_train[:,0], y_pred, color='red', linewidth=3)

plt.xlabel('Promedio Proyecto')
plt.ylabel('Numero de Tareas')
plt.title('Regresión Lineal')
y_Dosmil = regr.predict([[60]])
print("Grupos a abrir: ",int(y_Dosmil))


plt.show()



#Graficación del segundo modelo en 3D
promedio = ((filtered_data["Examen1"] + filtered_data['Examen2'] + filtered_data['Proyecto'])/3)

dataX2 =  pd.DataFrame()

dataX2["Ntareas"] = filtered_data["Ntareas"]
dataX2["promedio"] = promedio
XY_train = np.array(dataX2)
z_train = filtered_data['Asistencia'].values

                        # Creamos un nuevo objeto de Regresión Lineal
regr2 = linear_model.LinearRegression()

# Entrenamos el modelo, esta vez, con 2 dimensiones
# obtendremos 2 coeficientes, para graficar un plano
regr2.fit(XY_train, z_train)

# Hacemos la predicción con la que tendremos puntos sobre el plano hallado
z_pred = regr2.predict(XY_train)

# Los coeficientes
print('Coeficientes: \n', regr2.coef_)
# Error cuadrático medio
print("Error Cuadrático medio: %.2f" % mean_squared_error(z_train, z_pred))
# Evaluamos el puntaje de varianza (siendo 1.0 el mejor posible)
print('Valor de la Varianza: %.2f' % r2_score(z_train, z_pred))

fig = plt.figure()
ax = Axes3D(fig)

# Creamos una malla, sobre la cual graficaremos el plano
xx, yy = np.meshgrid(np.linspace(0, 3500, num=10), np.linspace(0, 60, num=10))

# calculamos los valores del plano para los puntos x e y
nuevoX = (regr2.coef_[0] * xx)
nuevoY = (regr2.coef_[1] * yy)

# calculamos los correspondientes valores para z. Debemos sumar el punto de intercepción
z = (nuevoX + nuevoY + regr2.intercept_)

# Graficamos el plano
ax.plot_surface(xx, yy, z, alpha=0.2, cmap='hot')

# Graficamos en azul los puntos en 3D
ax.scatter(XY_train[:, 0], XY_train[:, 1], z_train, c='blue',s=30)

# Graficamos en rojo, los puntos que
ax.scatter(XY_train[:, 0], XY_train[:, 1], z_pred, c='red',s=40)

# con esto situamos la "camara" con la que visualizamos
ax.view_init(elev=30., azim=65)

ax.set_xlabel('Asistencia')
ax.set_ylabel('Promedio General')
ax.set_zlabel('Numero de tareas')
ax.set_title('Regresión Lineal con Múltiples Variables')


#Prediccion con 70 de promedio y 4 tareas

z_Dosmil = regr2.predict([[50, 40]])
print("Cuantos grupos abrir: ",int(z_Dosmil))

# Restamos los errores calculados antes:
# Obviamente, "menos error" es mejor
mejoraEnError = mean_squared_error(y_train, y_pred) - mean_squared_error(z_train, z_pred)
print("Mejora en el error: ", mejoraEnError)

# También calculamos la mejora en la varianza:
mejoraEnVarianza = r2_score(z_train, z_pred) - r2_score(y_train, y_pred)
print("Mejora en la varianza: ", mejoraEnVarianza)

# Finalmente, mejoramos en nuestra predicción de un promedio
# pues aunque disminuyen los "Shares" que obtendremos en el 2do modelo,
# seguramente será un valor más cercano a la realidad
diferenciaComparir = z_Dosmil - y_Dosmil
print("Diferencia con el primer modelo: ",int(diferenciaComparir))
